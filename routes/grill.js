const db = require('../db');
const express = require('express');

const authenticateToken = require('../middlewares/auth');

const grillRoutes = express.Router();

grillRoutes.get('/', async (req, res, next) => {
  const { equipment } = req.query;
  // get 4 statuses based on block: 1,2,3 and 4. Group by appartment block property, if any of speciffic block is 'on'so the block grill is on
  try {
    const apartments = await db.aggregate({
      collection: 'apartment',
      aggregation: [
        {
          $group: {
            _id: '$block',
            status: {
              $push: '$status',
            },
          },
        },
      ],
    });

    const status = apartments.map(apartment => {
      const { _id, status } = apartment;
      const isOn = status.includes('on');
      return {
        block: _id,
        status: isOn ? 'on' : 'off',
      };
    });

    if(equipment) {
      await db.updateOne({
        collection: 'equipment',
        query: {
          code: equipment,
        },
        update: {
          $set: {
            code: equipment,
            lastPing: new Date(),
          },
        },
        options: {
          upsert: true,
        }
      });
    }
    

    res.json(status);
  }
  catch (err) {
    next(err);
  }    
});

grillRoutes.put('/toggle', authenticateToken, async (req, res, next) => {
  try {
    const { number } = req.user;

    const apartment = await db.findOne({
      collection: 'apartment',
      query: { number },
    });

    await db.updateMany({
      collection: 'apartment',
      query: {
        number
      },
      document: {
        $set: {
          status: apartment.status === 'off' ? 'on' : 'off',
        },
      },
    });

    res.json({ updated: true });
  } catch (err) {
    next(err);
  }
});

module.exports = grillRoutes;
