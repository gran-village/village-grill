const db = require('../db');
const express = require('express');

const authenticateToken = require('../middlewares/auth');

const equipmentRoutes = express.Router();

equipmentRoutes.get('/:code', authenticateToken, async (req, res, next) => {
  try {
    const { code } = req.params;

    const equipment = await db.findOne({
      collection: 'equipment',
      query: {
        code,
      },
    });

    if (!equipment) {
      return res.status(404).json({ error: 'Equipment not found' });
    }

    const seconds = (new Date() - new Date(equipment.lastPing)) / 1000; 
    equipment.status = seconds > 20 ? 'offline' : 'online';

    res.json(equipment);
  } catch (err) {
    next(err);
  }
});

module.exports = equipmentRoutes;
