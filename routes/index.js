const express = require('express');

const apartmentRoutes = require('./apartment');
const grillRoutes = require('./grill');
const equipmentRoutes = require('./equipment');

const privateRoutes = express.Router();

privateRoutes.use('/apartment', apartmentRoutes);
privateRoutes.use('/grill', grillRoutes);
privateRoutes.use('/equipment', equipmentRoutes);

module.exports = privateRoutes;