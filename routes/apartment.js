const express = require('express');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const mongo = require('mongodb');
const Hashids = require('hashids/cjs');

const db = require('../db');
const authenticateToken = require('../middlewares/auth');

const apartmentRoutes = express.Router();
const hashids = new Hashids('granvillage', 10);

apartmentRoutes.post('/create-all-apartments', async (req, res, next) => {
  try {
    const apartments = [];

    for (let i = 1; i <= 15; i++) {
      for (let k = 1; k <= 4; k++) {
        const number = `${i}${k}`;
        const block = k;
        const status = 'off';

        apartments.push({
          number,
          block,
          status,
          hashid: hashids.encode(number),
        });
      }
    }

    await db.insertMany({
      collection: 'apartment',
      document: apartments,
    });

    res.json({ created: true });
  } catch (err) {
    next(err);
  }
});


apartmentRoutes.get('/', authenticateToken, async (req, res, next) => {
  try {
    const apartments = await db.findAll({
      collection: 'apartment',
      query: {},
    });

    const appartmentsWithoutPassword = apartments.map(apartment => {
      const { password, hashid, ...rest } = apartment;
      return rest;
    });

    res.json(appartmentsWithoutPassword);
  } catch (err) {
    next(err);
  }
});

apartmentRoutes.get('/my-apartment', authenticateToken, async (req, res, next) => {
  try {
    const { number } = req.user;

    const apartment = await db.findOne({
      collection: 'apartment',
      query: { number },
    });

    if (!apartment) {
      return res.status(404).json({ error: 'Apartment not found' });
    }

    const { password, hash, ...rest } = apartment;

    res.json(rest);
  } catch (err) {
    next(err);
  }
});

apartmentRoutes.put('/set-password', async (req, res, next) => {
  try {
    const { password, hash } = req.body;

    const apartment = await db.findOne({
      collection: 'apartment',
      query: { hashid: hash },
    });

    if (!apartment) {
      return res.status(404).json({ error: 'Apartment not found' });
    }

    if (apartment.password) {
      return res.status(400).json({ error: 'Password already set' });
    }

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(password, salt);

    await db.updateMany({
      collection: 'apartment',
      query: {
        hashid: hash,
      },
      document: {
        $set: {
          password: hashPassword,
        },
      },
    });

    res.json({ updated: true });
  } catch (err) {
    next(err);
  }
});

apartmentRoutes.post('/login', async (req, res, next) => {
  try {
    const { number, password } = req.body;

    const apartment = await db.findOne({
      collection: 'apartment',
      query: { number },
    });

    if (!apartment) {
      return res.status(404).json({ error: 'Apartment not found' });
    }

    const match = await bcrypt.compare(password, apartment.password);

    if (!match) {
      return res.status(400).json({ error: 'Invalid password' });
    }

    const token = jsonwebtoken.sign({
      number,
    }, process.env.jwtSecret);

    res.json({ logged: true, token});
  } catch (err) {
    next(err);
  }
});

apartmentRoutes.get('/:hashid', async (req, res, next) => {
  try {
    const { hashid } = req.params;

    const apartment = await db.findOne({
      collection: 'apartment',
      query: {
        hashid,
        password: { $in: [null, undefined] },
      },
    });

    if (!apartment) {
      return res.status(404).json({ error: 'Apartment not found or password has been setted' });
    }

    const { password, ...rest } = apartment;

    res.json(rest);
  } catch (err) {
    next(err);
  }
});

module.exports = apartmentRoutes;
