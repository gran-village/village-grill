const mongoClient = require('mongodb').MongoClient;

const uri = process.env.mongoUri;

mongoClient.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
})
  .then(conn => global.conn = conn.db("village-grill"))
  .catch(err => console.log(err));
 
function findAll({ collection, query }) {
  return global.conn.collection(collection).find(query).toArray();
}

function insert({ collection, document }) {
  return global.conn.collection(collection).insertOne(document);
}

function insertMany({ collection, document }) {
  return global.conn.collection(collection).insertMany(document);
}

function findOne({ collection, query }) {
  return global.conn.collection(collection).findOne(query);
}

function updateOne({ collection, update, query, options = {} }) {  
  return global.conn.collection(collection).updateOne(query, update, options);
}

function updateMany({ collection, query, document }) {
  return global.conn.collection(collection).updateMany(query, document);
}

function aggregate({ collection, aggregation }) {
  return global.conn.collection(collection).aggregate(aggregation).toArray();
}
 
module.exports = {
  findAll,
  insert,
  insertMany,
  findOne,
  updateOne,
  updateMany,
  aggregate,
};