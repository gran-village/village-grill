require('dotenv').config();

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const routes = require('./routes');

const app = express();

app.use(cors());
app.use(bodyParser.json());

const port = process.env.PORT || 5000;

app.get('/', (_, res) => {
  res.json({
    status: 'OK',
  });
});

app.use('/', routes);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
})